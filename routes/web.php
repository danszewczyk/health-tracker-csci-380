<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
    return view('static.home');
});

Route::get('/about', function() {
    return view('static.about');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/diets', 'UserDietController@store')->name('user.diets.store');
Route::post('/diets/{diet}', 'UserDietController@end')->name('user.diets.end');
Route::post('/log', 'UserLogController@store')->name('user.log.store');