@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">


        <div class="col-md-7">

            <div class="card mb-3">
                <div class="card-header d-flex">
                    <div class="mr-auto mb-0 h3 align-self-center">Health Log</div>
                    <div class="btn btn-primary align-self-center" data-toggle="modal" data-target="#add_activity">Add</div>
                </div>

                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif


                    @foreach(auth()->user()->fitness_transactions as $fitness_transaction)
                        <div class="media mb-3 pb-3 border-bottom">
                            <div class="pr-3" style="min-width: 120px;">
                                <h3 class="mb-1">{{ $fitness_transaction->health_indicator_value }} <small class="text-muted">{{ $fitness_transaction->healthIndicator->unit_of_measure }}</small></h3>
                            </div>
                            <div class="media-body pt-2">
                                <h5 class="mt-0 mb-0">Your <strong>{{ $fitness_transaction->healthIndicator->name }}</strong> on {{ $fitness_transaction->created_at->toFormattedDateString() }}</h5>
                                <small>{{ $fitness_transaction->created_at->toDateTimeString() }}</small>
                            </div>
                        </div>
                    @endforeach


                </div>
            </div>

        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">My Profile</div>

                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-6">Name</dt>
                        <dd class="col-sm-6">{{ auth()->user()->name }}</dd>

                        <dt class="col-sm-6">Date of birth</dt>
                        <dd class="col-sm-6">{{ \Carbon\Carbon::parse(auth()->user()->date_of_birth)->toFormattedDateString() }}</dd>

                        <dt class="col-sm-6">Gender</dt>
                        <dd class="col-sm-6">@if(auth()->user()->gender == 'm') Male @else Female @endif</dd>
                    </dl>

                    <dl class="row">
                        <dt class="col-sm-6"><span data-toggle="tooltip" data-placement="top" title="Body Mass Index">BMI</span></dt>
                        <dd class="col-sm-6">{{ auth()->user()->calculateBMI() }}</dd>

                        <dt class="col-sm-6"><span data-toggle="tooltip" data-placement="top" title="Basal Metabolic Rate">BMR</span></dt>
                        <dd class="col-sm-6">{{ number_format(auth()->user()->calculateBMR(), 2) }}</dd>

                        <dt class="col-sm-6"><span data-toggle="tooltip" data-placement="top" title="Total Daily Energy Expenditure">TDEE</span></dt>
                        <dd class="col-sm-6">{{ auth()->user()->calculateNecessaryCalories() }}</dd>
                    </dl>

                </div>

            </div>

            <div class="card mt-3">
                <div class="card-header">My Diets</div>

                <div class="list-group list-group-flush">
                    @forelse(auth()->user()->diets as $diet)
                            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center" data-toggle="modal" data-target="#UserDietModal-{{ $diet->id }}">
                                <span>
                                    {{ $diet->name }}<br/>
                                    <small>
                                        @empty($diet->pivot->ended_at)
                                        Started on {{ $diet->pivot->started_at }}
                                        @else
                                        Ended on {{ $diet->pivot->ended_at }}
                                        @endempty
                                    </small>
                                </span>

                                @empty($diet->pivot->ended_at)
                                    <span class="badge badge-primary badge-pill">Active</span>
                                @endempty
                            </a>

                    @empty
                        <div class="list-group-item text-center py-4">
                            <p class="mb-1">You haven't tried out a diet yet!</p>
                        </div>
                    @endempty
                </div>
            </div>

            @if(!auth()->user()->hasActiveDiet())
             <a href="#" class="btn btn-block btn-outline-primary mt-2" data-toggle="modal" data-target="#diets">Start a new diet</a>
            @endif
        </div>


    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="diets" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Select a diet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="accordion">
                    @forelse ($diets as $diet)

                        <div class="card mb-2">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-{{ $diet->id }}" aria-expanded="true" aria-controls="collapse-{{ $diet->id }}">
                                        {{ $diet->name }}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse-{{ $diet->id }}" class="collapse" aria-labelledby="heading-{{ $diet->id }}" data-parent="#accordion">
                                <div class="card-body">
                                    {{ $diet->description }}
                                </div>

                                <div class="p-3">
                                    <form action="{{ route('user.diets.store') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="diet_id" value="{{ $diet->id }}">
                                        <button type="submit" class="btn btn-primary btn-block">Start this diet</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="text-muted text-center">No diets :(</p>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@foreach(auth()->user()->diets as $diet)
<div class="modal fade" id="UserDietModal-{{ $diet->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">{{ $diet->name }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            <p>
                {{ $diet->description }}
            </p>

            <hr>

            <h3>Health Log</h3>

            @isset($diet->pivot->ended_at)

                @forelse(auth()->user()->fitness_transactions()->whereBetween('created_at', [$diet->pivot->started_at, $diet->pivot->ended_at])->get() as $fitness_transaction)
                    <div class="media mb-3 pb-3 border-bottom">
                        <div class="pr-3" style="min-width: 120px;">
                            <h3 class="mb-1">{{ $fitness_transaction->health_indicator_value }} <small class="text-muted">{{ $fitness_transaction->healthIndicator->unit_of_measure }}</small></h3>
                        </div>
                        <div class="media-body pt-2">
                            <h5 class="mt-0 mb-0">Your <strong>{{ $fitness_transaction->healthIndicator->name }}</strong> on {{ $fitness_transaction->created_at->toFormattedDateString() }}</h5>
                            <small>{{ $fitness_transaction->created_at->toDateTimeString() }}</small>
                        </div>
                    </div>
                @empty
                    <p>No records to show yet. Add something to your Health Log.</p>
                @endforelse

            @else

                @forelse(auth()->user()->fitness_transactions()->where('created_at', '>', $diet->pivot->started_at)->get() as $fitness_transaction)
                    <div class="media mb-3 pb-3 border-bottom">
                        <div class="pr-3" style="min-width: 120px;">
                            <h3 class="mb-1">{{ $fitness_transaction->health_indicator_value }} <small class="text-muted">{{ $fitness_transaction->healthIndicator->unit_of_measure }}</small></h3>
                        </div>
                        <div class="media-body pt-2">
                            <h5 class="mt-0 mb-0">Your <strong>{{ $fitness_transaction->healthIndicator->name }}</strong> on {{ $fitness_transaction->created_at->toFormattedDateString() }}</h5>
                            <small>{{ $fitness_transaction->created_at->toDateTimeString() }}</small>
                        </div>
                    </div>
                @empty
                    <p class="text-muted text-center">No records to show yet. Add something to your Health Log.</p>
                @endforelse
            @endif

            <ul class="list-group">
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <span><strong>Started on</strong> {{ $diet->pivot->started_at }}</span>
                    <form action="{{ route('user.diets.end', $diet->id) }}" method="post">
                        @csrf
                        @empty($diet->pivot->ended_at)
                        <button type="submit" class="btn btn-danger">End this diet</button>
                        @endempty
                    </form>
                </li>

                @isset($diet->pivot->ended_at)
                    <li class="list-group-item"><strong>Ended on</strong> {{ $diet->pivot->ended_at }}</li>
                @endisset
            </ul>
        </div>
    </div>
</div>
</div>
@endforeach


<!-- Add Activity Modal -->
<div class="modal fade" id="add_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add to your health log</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ route('user.log.store') }}">

            <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <label for="health_indicator_id">Health indicator</label>
                        <select class="form-control" id="health_indicator_id" name="health_indicator_id">
                            <option value="">Select a health indicator</option>
                            @foreach($health_indicators as $health_indicator)
                                <option value="{{ $health_indicator->id }}">{{ $health_indicator->name }} ({{ $health_indicator->unit_of_measure }})</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlInput1">Value</label>
                        <input type="text" class="form-control" id="health_indicator_value" name="health_indicator_value">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Add to log</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </form>
        </div>
    </div>
</div>



@endsection
