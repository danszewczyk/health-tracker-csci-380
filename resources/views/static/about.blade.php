<!DOCTYPE HTML>
<html lang = "en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- default styles -->
    <link rel = "stylesheet" type = "text/css" href="style.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title> Report</title>

    <style>

        nav ul a {
            color: black !important;
            font-family: 'Poiret One' cursive;
            font-size: 22px !important;
            font-weight: 200;
        }

        .myNav {
            background-color: transparent !important;
            border-bottom: 1px solid grey;
            background: rgba(70, 72, 82, 0.3) !important;
        }

        html, body {
            height: 100%;
            width: 100%;
        }

        .background {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            background-attachment: fixed;
            height: 100%;
            width: auto;
        }

        .background-fit {
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            background-attachment: fixed;
            height: auto;
            width: auto;
        }

        #health-into {
            background-image: url("images/bg-1-tent-pic.jpg");
            opacity: .2;
        }

        #phrase-open {
            position: relative;
            top: 30%;
            text-align: center;
            color: white;
            font-size: 1.5em;
            font-family: 'Quicksand', sans-serif;
            letter-spacing: 1px;
        }

        .redirect-page {
            color: white;
            background-color: rgba(0, 0, 0, .2);
            padding: 15px;
            font-size: 1em;
            border-radius: 15px;
            margin-top: 2%;
            border: 1px solid rgba(255, 255, 255, .2);
        }

        #heading-item {
            background-image: url("images/black-bricks.jpg");
            font-size: 1.5em;
            text-align: center;
            color: white;
            padding-top: 8%;
            padding-bottom: 8%;
            font-family: 'Quicksand', sans-serif;
            letter-spacing: 1px;
        }

        #dietInfo {
            background-image: url("images/food-pic-bg.jpg");
        }

        #services {
            background-image: url("images/ezgif.com-optimize.gif");
        }

        .diet-heading {
            text-align: center;
            color: white;
            border-radius: 3px;
        }

        .diet-info {
            color: white;
            border-top: 1px solid #F0F0F0;
        }

        .diet-health-healthy {
            border: 2px solid #008000;
        }

        .diet-health-moderate {
            border: 2px solid #0000ff;
        }

        .diet-health-unhealthy {
            border: 2px solid #e50000;
        }

        .diet-food-items {
            border-radius: 3px;
            color: white;
            border-top: 1px solid #F0F0F0;
        }

        .shadow1 {
            -webkit-box-shadow: 0px -5px 3px 0px rgba(0,0,0,0.75);
            -moz-box-shadow: 0px -5px 3px 0px rgba(0,0,0,0.75);
            box-shadow: 0px 0px 3px 0px rgba(0,0,0,0.75);
            background-color: rgba(0,153,255, .7);
        }

        .seperate {
            margin-top: 1.5em;
        }


    </style>

</head>

<script>
    $(document).ready(function() {
        //create a fadeIn effect for the page
        $("#health-into").fadeTo(1200, 1);
    });

</script>


<body>



<div class="myNav">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="/"> <img src="images/logoSmall.png" height="50" width="80"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/login" style="color: black">Login</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Sign Up <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about">About</a>
                </li>
            </ul>

        </div>
    </nav>
</div>

<div class="background" id = "health-into">
    <div class = "container" id = "phrase-open">
        Health Aware Helps Make You Into the Best Version of Yourself;<br> So You Can Enjoy Moments Like These
        <a href = "" style = "color: white;">
            <div class = "redirect-page">
                Start Your Journey to a Better You
            </div>
        </a>
    </div>

</div>

<div class = "background-fit" id = "heading-item">
    We Choose Only The Best Diets For Our Clients
    <br>
    Just See For Yourself
</div>

<div class = "background-fit" id = "dietInfo">
    <div class = "container" id = "diet-open">

        <div class="container">
            <div class="row">
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Atkins Diet</div>
                    <div class = "diet-info">
                        Atkins is a low carbohydrate diet. Atkins eliminates sugar highs and sugar lows to enable a constant blood sugar level. During sugar highs any extra sugar is stored as fat, while during sugar lows your body craves sugar and other carbohydrates. By keeping a steady blood sugar people on an Atkins diet avoid sugar highs which lead to weight gain. Another great part about Atkins is that it doesn't emphasize counting calories which makes this a great low carb diet.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: chicken, beef, bacon, turkey, Salmon, trout, flounder, eggs, egg whites, Kale, spinach, broccoli, nuts, seeds, and anything else low in carbohydrates!
                        <br>
                        Drinks of This Diet: water, coffee, and tea. Absolutely no beer.
                    </div>
                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">The Zone Diet</div>
                    <div class = "diet-info">
                        The Zone diet is based on a balance of carbohydrates, proteins, and fats. During each meal and snack the foods should be 1/3 protein, 2/3 carbohydrates, and with a slight amount of healthy fat. The Zone diet's carbohydrates come from vegetables and fish and little grains are allowed during the Zone diet. The Zone diet will help one lose weight, maintain wellness, improve athletic performance, and make on healthier.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: fish, turkey, skinless chicken, tofu, egg whites, or any other high protein foods, brown rice, oatmeal, whole wheat bread.
                        <br>
                        Drinks of This Diet: water, coffee, and tea.
                    </div>

                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Ketogenic Diet</div>
                    <div class = "diet-info">
                        The Ketogenic Diet involves increasing one's fat intake. This allows the body to burn fat rather than carbohydrates. This diet is typically used to treat medical conditions so it's typically not recommended for general use. This diet should be avoided for anyone with diabetes due to medical risks.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: Avacados, coconuts, seeds, olive oil, nuts, clams, mussels, Salmon, spinach, cheese, meat, eggs, and greek yogurt.
                        <br>
                        Drinks of This Diet: water, coffee, and tea. Alcohol use is not recommended in the ketogenic diet.
                    </div>
                </div>
            </div>
        </div>

        <div class="container seperate">
            <div class="row">
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading ">Vegetarian Diet</div>
                    <div class = "diet-info">
                        The Vegetarian diet is one of the most well known diets. This diet restricts the animal-based foods that one can eat. The only animal-based foods one can eat are eggs and dairy. Vegetarians have a lower body weight and live longer than those who eat meat.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: Any non animal-based foods except for dairy and eggs.
                        <br>
                        Drinks of This Diet: water, coffee, tea, juice, smoothies, and other fruit or vegetable based drinks.
                    </div>
                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Vegan Diet</div>
                    <div class = "diet-info">
                        Unlike the Vegetarian diet which allows some animal-based foods, the vegan diet completely restricts anything that is animal-based including eggs, honey, and dairy products. People who are on this diet experience the health benefits of the Vegetarian diet, while also not eating any animal-based foods at all.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: Any non animal-based foods. Including: Any fruits, and vegetables.
                        <br>
                        Drinks of This Diet: water, coffee, tea, juice, smoothies, and other fruit or vegetable based drinks.
                    </div>

                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Weight Watchers Diet</div>
                    <div class = "diet-info">
                        Unlike the other mentioned diets the Weight Watchers diet relies on a support network of peers who want to lose weight. This support network provides positive help to those struggling to lose weight. In these meetings people on the Weight Watchers diet learn about exercise, nutrition, and other healthy lifestyle habits. Furthermore, unlike diets such as, Atkins and Ketogenic, the Weight Watchers diet assigns points to each food eaten and has no dietary restrictions as long as the points meet the goal. For more info and to keep track of your points visit: https://www.weightwatchers.com.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: Anything as the long as the goal of points are met.
                        <br>
                        Drinks of This Diet: Anything as the long as the goal of points are met.
                    </div>
                </div>
            </div>
        </div>


        <div class="container seperate">
            <div class="row">
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">South Beach Diet</div>
                    <div class = "diet-info">
                        The South Beach Diet is based on the selecting the correct foods based on their glycemic index. The diet focuses on selecting the right carbohydrates. The diet aims at keeping one's blood sugar low, which in turn will cause one to eat less. This diet is useful for losing weight long term.
                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: chicken, turkey, fish, tofu, eggs, cheese, nuts, beans, vegetables, and certain low carb fruits.
                        <br>
                        Drinks of This Diet: water, coffee, tea, and absolutely no soda.
                    </div>
                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Raw Food Diet</div>
                    <div class = "diet-info">
                        The Raw food diet centers around foods that are completely raw. The benefits include weight loss, better overall health, and other benefits. Followers of this diet believe that cooking food destroys the natural benefits of the food. Due to eating all raw food this diet can be dangerous to one's health, but it can also lead to significant weight loss.
                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: all fresh vegetables, all raw vegetables, raw nuts, seeds, dried fruits, dried meats, seaweed, sauerkraut, kimchi, raw eggs, raw dairy, raw meat, raw fish, sushi, and oats.
                        <br>
                        Drinks of This Diet: water, smoothies, coffee, lemonade, and fruit juices.
                    </div>

                </div>
                <div class="col-sm-4 shadow1">
                    <div class = "diet-heading">Mediterranean Diet</div>
                    <div class = "diet-info">
                        The Mediterranean diet consists of food from Italy, Greece, Spain, and Portugal. The Mediterranean diet avoids highly processed meats and foods, foods high in trans fats, and foods with a lot of oils. The Mediterranean diet is linked to lower obesity rates, diabetes, and cancer rates.

                    </div>
                    <div class = "diet-food-items">
                        Foods of This Diet Include: Tomatoes, kale, broccoli, spinach, carrots, cucumbers, onions, apples, bananas, figs, dates, grapes, melons, beans, nuts, seeds, almonds, walnuts, cashews, oats, brown rice, olive oil, cheese, yogurt, duckm turkey, salmon, eggs, and very little sweets.
                        <br>
                        Drinks of This Diet: water, smoothies, coffee, wine, and tea.
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

<div class = "background-fit" id = "heading-item">
    The Services We Offer Are Unbeatable
</div>



<div class = "background" id = "services">
    <div class="container">
        <div class = "row" style = "text-align: center; padding-top: 25%">
            <div class="col-sm-4 shadow1" style = "padding-bottom: 25%;padding-top: 10vh;
padding-bottom: 10vh;
color: wheat;" >
                Record Your Vitals
            </div>
            <div class="col-sm-4 shadow1" style = "padding-bottom: 25%;padding-top: 10vh;
padding-bottom: 10vh;
color: wheat;" >
                Check Your BMI and total calories needed
            </div>
            <div class="col-sm-4 shadow1" style = "padding-bottom: 25%;padding-top: 10vh;
padding-bottom: 10vh;
color: wheat;" >
                Choose a diet and monitor your progress
            </div>
        </div>
        <div>
            <a href = "" style = "color: white; text-align: center">
                <div class = "redirect-page">
                    Start Your Journey to a Better You
                </div>
            </a>
        </div>
    </div>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
</body>
</html>