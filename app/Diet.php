<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diet extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    // Relationships
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function foods()
    {
        // TODO: make the foods table
        //return $this->belongsToMany(Food:class);
    }

}
