<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'date_of_birth', 'gender', 'activity_level_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Relationships
    public function diets()
    {
        return $this->belongsToMany(Diet::class)->withPivot('started_at', 'ended_at')->orderByDesc('id');
    }

    public function activityLevel()
    {
        return $this->belongsTo(ActivityLevel::class);
    }

    public function fitness_transactions()
    {
        return $this->hasMany(FitnessTransaction::class)->latest();
    }

    public function hasActiveDiet()
    {
        return (bool) $this->diets()->wherePivot('ended_at', '=', null)->count();
    }

    public function recentIndicator($indicator)
    {
        return $this->fitness_transactions()->whereHas('healthIndicator', function ($query) use ($indicator) {
            $query->where('name', '=', $indicator);
        })->latest()->first();
    }

    public function age()
    {
        return Carbon::parse($this->date_of_birth)->age;
    }


    //calculates the BMR (The ratio or weight,height, age, and gender) depending on the height, weight, age, and gender
    public function calculateBMR() {

        $height = optional($this->recentIndicator('height'))->health_indicator_value;
        $weight = optional($this->recentIndicator('weight'))->health_indicator_value;
        $age = $this->age();
        $gender = $this->gender;

        if ($height == 0 || $weight == 0) {
            return false;
        } else {
            //Male BMR formula: BMR = 66 + (6.23 x weight in pounds) + (12.7 x height in inches) - (6.8 x age in years)
            if($gender==="m") {
                $BMR = 66 + (6.23*$weight) + (12.7 * $height) - (6.8*$age);
            }
            //Women BMR formula: BMR = 655 + (4.35 x weight in pounds) + (4.7 x height in inches) - (4.7 x age in years)
            else {
                $BMR = 655 + (4.35*$weight) + (4.7*$height) - (4.7*$age);
            }
            return $BMR;
        }

    }

    function calculateNecessaryCalories() {

        $height = optional($this->recentIndicator('height'))->health_indicator_value;
        $weight = optional($this->recentIndicator('weight'))->health_indicator_value;
        $age = $this->age();
        $gender = $this->gender;
        $activityLevel = $this->activityLevel()->first()->name;

        $BMR = $this->calculateBMR();
        $necessaryCalories = 0;
        if($activityLevel === "Sedentary") {
            $necessaryCalories = $BMR*1.2;
        }
        elseif($activityLevel === "Lightly active") {
            $necessaryCalories = $BMR*1.375;
        }
        elseif($activityLevel === "Moderately active") {
            $necessaryCalories = $BMR*1.55;
        }
        elseif($activityLevel === "Very active") {
            $necessaryCalories = $BMR*1.725;
        }
        //Extremely active
        else{
            $necessaryCalories = $BMR*1.9;
        }
        //return an integer of the calories that can be eaten
        return floor($necessaryCalories);
    }

    public function calculateBMI()
    {
        $height = optional($this->recentIndicator('height'))->health_indicator_value;
        $weight = optional($this->recentIndicator('weight'))->health_indicator_value;

        if ($height == 0 || $weight == 0) {
            return false;
        } else {
            $height_sqaured = pow(intval($height), 2);
            $bmi = ($weight / $height_sqaured) * 703;
        }


        /*
         * Severe Thinness	< 16
            Moderate Thinness	16 - 17
            Mild Thinness	17 - 18.5
            Normal	18.5 - 25
            Overweight	25 - 30
            Obese Class I	30 - 35
            Obese Class II	35 - 40
            Obese Class III	> 40
         */

        if ($bmi < 16) {
            $status = "Severe Thinness";
        } else if ($bmi < 17) {
            $status = "Moderate Thinness";
        } else if ($bmi < 18.5) {
            $status = "Mild Thinness";
        } else if ($bmi < 25) {
            $status = "Normal";
        } else if ($bmi < 30) {
            $status = "Overweight";
        } else if ($bmi < 35) {
            $status = "Overweight Class I";
        } else if ($bmi < 40) {
            $status = "Obese Class II";
        } else if ($bmi > 40) {
            $status = "Obese Class III";
        }

        return number_format($bmi, 2) . " ($status)";
    }



}
