<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FitnessTransaction extends Model
{
    protected $table = 'fitness_log_transactions';

    protected $fillable = ['health_indicator_id', 'health_indicator_value'];

    public function healthIndicator()
    {
        return $this->belongsTo(HealthIndicator::class);
    }

}
