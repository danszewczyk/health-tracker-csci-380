<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserLogController extends Controller
{
    public function store(Request $request)
    {
        auth()->user()->fitness_transactions()->create([
            'health_indicator_id' => $request->health_indicator_id,
            'health_indicator_value' => $request->health_indicator_value
        ]);

        return redirect()->route('home')->withSuccess('Successfully added to your fitness log!');
    }
}
