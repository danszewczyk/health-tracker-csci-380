<?php

namespace App\Http\Controllers;

use App\Diet;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserDietController extends Controller
{
    public function store(Request $request)
    {
        auth()->user()->diets()->attach($request->diet_id, ['started_at' => Carbon::now()]);

        return redirect()->route('home')->with('Successfully started a new diet!');
    }

    public function end(Diet $diet)
    {
        auth()->user()->diets()->updateExistingPivot($diet->id, ['ended_at' => Carbon::now()]);

        return redirect()->route('home');
    }
}
