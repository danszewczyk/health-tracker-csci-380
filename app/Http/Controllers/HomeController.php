<?php

namespace App\Http\Controllers;

use App\Diet;
use App\HealthIndicator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $diets = Diet::get();
        $health_indicators = HealthIndicator::get();

        return view('home', compact('diets','health_indicators'));
    }
}
