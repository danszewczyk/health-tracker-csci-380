<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HealthIndicatorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $health_indicators = [
            [
                'name' => 'Height',
                'unit_of_measure' => 'in'
            ],
            [
                'name' => 'Weight',
                'unit_of_measure' => 'lbs.'
            ],
            [
                'name' => 'Heart rate',
                'unit_of_measure' => 'bpm'
            ],
            [
                'name' => 'Blood pressure',
                'unit_of_measure' => 'mmHg'
            ],
            [
                'name' => 'Blood sugar',
                'unit_of_measure' => 'mg/dL'
            ]
        ];

        DB::table('health_indicators')->insert($health_indicators);
    }
}
