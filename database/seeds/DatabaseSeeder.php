<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ActivityLevelTableSeeder::class);
        $this->call(DietsTableSeeder::class);
        $this->call(HealthIndicatorsTableSeeder::class);
    }
}
