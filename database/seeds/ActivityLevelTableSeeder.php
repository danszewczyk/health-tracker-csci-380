<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivityLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $activity_levels = [
            ['name' => 'Sedentary'],
            ['name' => 'Lightly active'],
            ['name' => 'Moderately active'],
            ['name' => 'Very active'],
        ];

        DB::table('activity_levels')->insert($activity_levels);
    }
}
