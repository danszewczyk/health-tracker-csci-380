<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DietsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $diets = [
            [
                'name' => 'Atkins Diet',
                'description' => 'Atkins is a low carbohydrate diet. Atkins eliminates sugar highs and sugar lows to enable a constant blood sugar level. During sugar highs any extra sugar is stored as fat, while during sugar lows your body craves sugar and other carbohydrates. By keeping a steady blood sugar people on an Atkins diet avoid sugar highs which lead to weight gain. Another great part about Atkins is that it doesn\'t emphasize counting calories which makes this a great low carb diet.
Foods of This Diet Include: chicken, beef, bacon, turkey, Salmon, trout, flounder, eggs, egg whites, Kale, spinach, broccoli, nuts, seeds, and anything else low in carbohydrates! 
Drinks of This Diet: water, coffee, and tea. Absolutely no beer.'
            ],
            [
                'name' => 'The Zone Diet',
                'description' => 'The Zone diet is based on a balance of carbohydrates, proteins, and fats. During each meal and snack the foods should be 1/3 protein, 2/3 carbohydrates, and with a slight amount of healthy fat. The Zone diet\'s carbohydrates come from vegetables and fish and little grains are allowed during the Zone diet. The Zone diet will help one lose weight, maintain wellness, improve athletic performance, and make on healthier.
Foods of This Diet Include: fish, turkey, skinless chicken, tofu, egg whites, or any other high protein foods, brown rice, oatmeal, whole wheat bread. 
Drinks of This Diet: water, coffee, and tea.',
            ],
            [
                'name' => 'Ketogenic Diet',
                'description' => 'The Ketogenic Diet involves increasing one\'s fat intake. This allows the body to burn fat rather than carbohydrates. This diet is typically used to treat medical conditions so it\'s typically not recommended for general use. This diet should be avoided for anyone with diabetes due to medical risks.
Foods of This Diet Include: Avacados, coconuts, seeds, olive oil, nuts, clams, mussels, Salmon, spinach, cheese, meat, eggs, and greek yogurt. 
Drinks of This Diet: water, coffee, and tea. Alcohol use is not recommended in the ketogenic diet.',
            ],
            [
                'name' => 'Vegetarian Diet',
                'description' => 'The Vegetarian diet is one of the most well known diets. This diet restricts the animal-based foods that one can eat. The only animal-based foods one can eat are eggs and dairy. Vegetarians have a lower body weight and live longer than those who eat meat.
Foods of This Diet Include: Any non animal-based foods except for dairy and eggs. 
Drinks of This Diet: water, coffee, tea, juice, smoothies, and other fruit or vegetable based drinks.',
            ],
            [
                'name' => 'Vegan Diet',
                'description' => 'Unlike the Vegetarian diet which allows some animal-based foods, the vegan diet completely restricts anything that is animal-based including eggs, honey, and dairy products. People who are on this diet experience the health benefits of the Vegetarian diet, while also not eating any animal-based foods at all.
Foods of This Diet Include: Any non animal-based foods. Including: Any fruits, and vegetables. 
Drinks of This Diet: water, coffee, tea, juice, smoothies, and other fruit or vegetable based drinks.',
            ],
            [
                'name' => 'Weight Watchers Diet',
                'description' => 'Unlike the other mentioned diets the Weight Watchers diet relies on a support network of peers who want to lose weight. This support network provides positive help to those struggling to lose weight. In these meetings people on the Weight Watchers diet learn about exercise, nutrition, and other healthy lifestyle habits. Furthermore, unlike diets such as, Atkins and Ketogenic, the Weight Watchers diet assigns points to each food eaten and has no dietary restrictions as long as the points meet the goal. For more info and to keep track of your points visit: https://www.weightwatchers.com.
Foods of This Diet Include: Anything as the long as the goal of points are met. 
Drinks of This Diet: Anything as the long as the goal of points are met.',
            ],
            [
                'name' => 'South Beach Diet',
                'description' => 'The South Beach Diet is based on the selecting the correct foods based on their glycemic index. The diet focuses on selecting the right carbohydrates. The diet aims at keeping one\'s blood sugar low, which in turn will cause one to eat less. This diet is useful for losing weight long term.
Foods of This Diet Include: chicken, turkey, fish, tofu, eggs, cheese, nuts, beans, vegetables, and certain low carb fruits. 
Drinks of This Diet: water, coffee, tea, and absolutely no soda.',
            ],
            [
                'name' => 'Raw Food Diet',
                'description' => 'The Raw food diet centers around foods that are completely raw. The benefits include weight loss, better overall health, and other benefits. Followers of this diet believe that cooking food destroys the natural benefits of the food. Due to eating all raw food this diet can be dangerous to one\'s health, but it can also lead to significant weight loss.
Foods of This Diet Include: all fresh vegetables, all raw vegetables, raw nuts, seeds, dried fruits, dried meats, seaweed, sauerkraut, kimchi, raw eggs, raw dairy, raw meat, raw fish, sushi, and oats. 
Drinks of This Diet: water, smoothies, coffee, lemonade, and fruit juices.',
            ],
            [
                'name' => 'Mediterranean Diet',
                'description' => 'The Mediterranean diet consists of food from Italy, Greece, Spain, and Portugal. The Mediterranean diet avoids highly processed meats and foods, foods high in trans fats, and foods with a lot of oils. The Mediterranean diet is linked to lower obesity rates, diabetes, and cancer rates.
Foods of This Diet Include: Tomatoes, kale, broccoli, spinach, carrots, cucumbers, onions, apples, bananas, figs, dates, grapes, melons, beans, nuts, seeds, almonds, walnuts, cashews, oats, brown rice, olive oil, cheese, yogurt, duckm turkey, salmon, eggs, and very little sweets. 
Drinks of This Diet: water, smoothies, coffee, wine, and tea.',
            ]
        ];

        DB::table('diets')->insert($diets);

    }
}
